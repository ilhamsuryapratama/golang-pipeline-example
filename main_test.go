package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHelloWorld(t *testing.T) {
	res := HelloWorld()

	assert.Equal(t, res, "Hello World")
}
